
/*
 * PROGRAM:
 *   MechanicalFixes - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   MechanicalFixes is a Bukkit plugin fixing some mechanical things.
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/

package fr.tobast.bukkit.mechanicalfixes;

import java.util.logging.Logger;
import org.bukkit.plugin.java.JavaPlugin;

import fr.tobast.bukkit.mechanicalfixes.DispenserReload;
import fr.tobast.bukkit.mechanicalfixes.SPistonMultiple;

public class MechanicalFixes extends JavaPlugin {
	Logger log=Logger.getLogger("Minecraft");
	
	public void onEnable() {
		loadConfig();
		
		if(getConfig().getBoolean("enable-dispenser-reload"))
			getServer().getPluginManager().registerEvents(new DispenserReload(), this);
		if(getConfig().getBoolean("enable-2spiston"))
			getServer().getPluginManager().registerEvents(new SPistonMultiple(this), this); // RELEASE
//			getServer().getPluginManager().registerEvents(new SPistonMultiple(this, log), this); // DEBUG

	}

	public void onDisable() {
		
	}

	public void loadConfig() {
		getConfig().options().copyDefaults(true);
		//def values
		getConfig().addDefault("enable-dispenser-reload", true);
		getConfig().addDefault("enable-2spiston", true);
		saveConfig();
	}
}
 
