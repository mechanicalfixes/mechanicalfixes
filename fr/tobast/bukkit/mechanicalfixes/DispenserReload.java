
/*
 * PROGRAM:
 *   MechanicalFixes - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   MechanicalFixes is a Bukkit plugin fixing some mechanical things.
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/

package fr.tobast.bukkit.mechanicalfixes;

import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.block.Block;
import org.bukkit.Material;
import org.bukkit.material.Dispenser;
import org.bukkit.block.BlockFace;


class DispenserReload implements Listener {
	@EventHandler(priority=EventPriority.HIGH) // shall be processed quickly
	public void onDispenserInteract(PlayerInteractEvent event) {
		Block clicked=event.getClickedBlock();

		if(clicked!=null && clicked.getType()==Material.DISPENSER) {
			Dispenser dispenser=new Dispenser(clicked.getType(), clicked.getData());
			BlockFace dispenserFront=dispenser.getFacing();
			
			if(event.getBlockFace() == dispenserFront) // Cannot reload from the front.
				event.setCancelled(true);
		}
	}
}

