
package fr.tobast.bukkit.mechanicalfixes;
/*
 * PROGRAM:
 *   MechanicalFixes - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   MechanicalFixes is a Bukkit plugin fixing some mechanical things.
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/


import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.Material;
import org.bukkit.material.PistonBaseMaterial;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.plugin.java.JavaPlugin;

// DEBUG
// import java.util.logging.Logger;

class SPistonMultiple implements Listener {
	JavaPlugin plugin;

/*	// DEBUG
	Logger log;
	public SPistonMultiple(JavaPlugin iPlugin, Logger iLog) {
		log=iLog;
		plugin=iPlugin;
	}
*/
	
	// RELEASE
	public SPistonMultiple(JavaPlugin iPlugin) {
		plugin=iPlugin;
	}

	@EventHandler(priority=EventPriority.HIGH)
	public void onPistonRetract(BlockPistonRetractEvent event) {
		if(!event.isSticky())
			return;

		Block piston=event.getBlock();
		if(piston != null) // just in case.
		{
			Block involved=piston.getRelative(event.getDirection(), 2); // At this stage, the piston isn't retracted yet.
			if(involved!=null && involved.getType() == Material.PISTON_STICKY_BASE)
			{
				PistonBaseMaterial involvedPiston = new PistonBaseMaterial(involved.getType(), involved.getData());
				if(involvedPiston.getFacing() == event.getDirection())
				{
					// get the possible stuck block
					Block stuck=piston.getRelative(event.getDirection(), 3);
					if(stuck!=null && stuck.getType() != Material.AIR)// Is a real block
					{
						plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new DelayedSPiston(involved, stuck, plugin), 5L); // 1/2 second
					}
				}
			}
		}
	}
}

