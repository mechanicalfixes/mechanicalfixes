
/*
 * PROGRAM:
 *   MechanicalFixes - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   MechanicalFixes is a Bukkit plugin fixing some mechanical things.
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
*/

package fr.tobast.bukkit.mechanicalfixes;

import org.bukkit.block.Block;
import org.bukkit.Material;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.plugin.java.JavaPlugin;

class DelayedSPiston implements Runnable {
	Block involved;
	Block oldPos;
	JavaPlugin plugin;
	int timesProcess;

	public DelayedSPiston(Block iBlock, Block iOldPos, JavaPlugin iPlugin) {
		involved=iBlock;
		oldPos=iOldPos;
		plugin=iPlugin;
		timesProcess=0;
	}
	public DelayedSPiston(Block iBlock, Block iOldPos, JavaPlugin iPlugin, int iTimesProcess) {
		involved=iBlock;
		oldPos=iOldPos;
		plugin=iPlugin;
		timesProcess=iTimesProcess;
	}


	public void run() {
		if(involved.getType() == Material.PISTON_EXTENSION) { // Not retracted yet.
			if(timesProcess < 3) {
				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new DelayedSPiston(involved, oldPos, plugin, timesProcess), 10L); // 3/4 second
			}
			return;
		}
		
		Material mat=oldPos.getType();
		oldPos.setType(Material.AIR);
		involved.setType(mat);
	}
}

